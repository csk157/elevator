import { describe, it } from 'mocha';
import Chai, { expect } from 'chai';
import spy from 'chai-spies';
import Elevator from '../src/Elevator.js';

Chai.use(spy);

describe('Elevator', () => {
  describe('Initialization', () => {
    const floors = ['K', 'S', '1', '2'];
    let elevator;

    beforeEach(() => {
      elevator = new Elevator(floors);
    });

    it('Initializes with and keeps an array of floors', () => {
      expect(elevator.floors.length).to.be.equal(floors.length);
    });

    it('Sets the first floor', () => {
      expect(elevator.currentIndex).to.be.equal(0);
    });

    it('Sets direction \'up\'', () => {
      expect(elevator.direction).to.be.equal('up');
    });

    it('Complains if list of floors is empty', () => {
      expect(() => new Elevator([])).to.throw(Error);
    });

    it('Complains if floors are not supplied', () => {
      expect(() => new Elevator()).to.throw(Error);
    });
  });

  describe('#getCurrentFloor', () => {
    const floors = ['K', 'S', '1', '2'];
    let elevator;

    beforeEach(() => {
      elevator = new Elevator(floors);
    });

    it('Returns the name of the current floor', () => {
      expect(elevator.getCurrentFloor()).to.be.equal('K');

      elevator.currentIndex = 1;
      expect(elevator.getCurrentFloor()).to.be.equal('S');
    });
  });

  describe('#move', () => {
    const floors = ['K', 'S', '1', '2'];
    let elevator;

    beforeEach(() => {
      elevator = new Elevator(floors);
    });

    it('Moves up when direction is up', () => {
      elevator.direction = 'up';

      elevator.move();
      expect(elevator.getCurrentFloor()).to.be.equal('S');

      elevator.move();
      expect(elevator.getCurrentFloor()).to.be.equal('1');
    });

    it('Moves down when direction is down', () => {
      elevator.goTo('1');
      elevator.direction = 'down';

      elevator.move();
      expect(elevator.getCurrentFloor()).to.be.equal('S');

      elevator.move();
      expect(elevator.getCurrentFloor()).to.be.equal('K');
    });

    it('Changes direction when it hits the top', () => {
      elevator.goTo('1');
      elevator.direction = 'up';

      elevator.move();
      expect(elevator.direction).to.be.equal('down');
    });

    it('Changes direction when it hits the bottom', () => {
      elevator.goTo('S');
      elevator.direction = 'down';

      elevator.move();
      expect(elevator.direction).to.be.equal('up');
    });
  });

  describe('#goTo', () => {
    const floors = ['K', 'S', '1', '2'];
    let elevator;

    beforeEach(() => {
      elevator = new Elevator(floors);
    });

    it('Does nothing if the floor with the given name does not exist', () => {
      const currentFloor = elevator.getCurrentFloor();
      elevator.goTo('1337');
      expect(elevator.getCurrentFloor()).to.be.equal(currentFloor);
    });

    it('Goes to the right floor', () => {
      elevator.goTo('2');
      expect(elevator.getCurrentFloor()).to.be.equal('2');
    });

    it('Calls #move', () => {
      Chai.spy.on(elevator, 'move');
      elevator.goTo('1');

      expect(elevator.move).to.have.been.called();
    });
  });

});
