import React from 'react';

export default class ElevatorDisplay extends React.Component {
  render() {
    return (<div className="display">
      <div className={`direction ${this.props.direction === 'up' && 'active'}`}>
        <i className="fa fa-angle-up"></i>
      </div>

      <div className="current-floor">{this.props.currentFloor}</div>

      <div className={`direction ${this.props.direction === 'down' && 'active'}`}>
        <i className="fa fa-angle-down"></i>
      </div>
    </div>);
  }
}

ElevatorDisplay.propTypes = {
  currentFloor: React.PropTypes.string.isRequired,
  direction: React.PropTypes.string.isRequired
};
