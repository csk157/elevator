import '../css/styles.scss';

import React from 'react';
import ReactDOM from 'react-dom';
import ElevatorComponent from './ElevatorComponent.jsx';

ReactDOM.render(<ElevatorComponent />, document.getElementById('container'));
