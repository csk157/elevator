import React from 'react';
import Elevator from './Elevator.js';
import ElevatorKeyboard from './ElevatorKeyboard.jsx';
import ElevatorDisplay from './ElevatorDisplay.jsx';

export default class ElevatorComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      lastAction: 'You haven\'t clicked any button!',
      elevator: new Elevator(['-53', 'P2', 'P1', 'S', '1', '2', '3'])
    };
  }

  goToFloor(name) {
    this.setState({
      lastAction: `You went from ${this.state.elevator.getCurrentFloor()} to ${name}`
    });

    this.state.elevator.goTo(name);
  }

  render() {
    const onFloorClickFn = (name) => this.goToFloor(name);

    return (<div className="elevator">
      <ElevatorDisplay
        currentFloor={this.state.elevator.getCurrentFloor()}
        direction={this.state.elevator.direction}
      />

      <ElevatorKeyboard
        floors={this.state.elevator.floors}
        onFloorClickFn={onFloorClickFn}
      />
      <p>{this.state.lastAction}</p>
    </div>);
  }
}
