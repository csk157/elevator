export default class Elevator {
  constructor(floors) {
    if (!floors || floors.length === 0) {
      throw new Error('List of floors must be set and must not be empty');
    }

    this.floors = floors;
    this.currentIndex = 0;
    this.direction = 'up';
  }

  getCurrentFloor() {
    return this.floors[this.currentIndex];
  }

  goTo(name) {
    const targetIndex = this.floors.indexOf(name);

    if (targetIndex === -1 || targetIndex === this.currentIndex) {
      return;
    }

    this.direction = (targetIndex < this.currentIndex) ? 'down' : 'up';

    let moves = Math.abs(this.currentIndex - targetIndex);

    while (moves > 0) {
      this.move();
      moves--;
    }
  }

  move() {
    const movement = (this.direction === 'up') ? 1 : -1;
    this.currentIndex += movement;

    if (this.currentIndex === 0) {
      this.direction = 'up';
    } else if (this.currentIndex === (this.floors.length - 1)) {
      this.direction = 'down';
    }

  }
}
