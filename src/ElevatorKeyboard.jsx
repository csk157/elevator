import React from 'react';

export default class ElevatorKeyboard extends React.Component {
  onClickFn(name) {
    return () => {
      this.props.onFloorClickFn(name);
    };
  }

  render() {
    const buttons = this.props.floors.map((name) =>
      <button key={`floor-${name}`} onClick={this.onClickFn(name)}>{name}</button>);
    return <div className="keyboard">{buttons.reverse()}</div>;
  }
}

ElevatorKeyboard.propTypes = {
  floors: React.PropTypes.array.isRequired,
  onFloorClickFn: React.PropTypes.func.isRequired
};
