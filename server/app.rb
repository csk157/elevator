require 'rubygems'
require 'sinatra'

get '/' do
  File.read(File.join('server', 'public', 'index.html'))
end
